
from PyQt5 import QtWidgets, uic
from helper import *
import sys

class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('main.ui', self) # Load the .ui file
        self.btn1.setText("Find")
        self.btn1.clicked.connect(self.calcValues)

        self.show() # Show the GUI

    def calcValues(self):
    	balance = float(self.balanceTxt.text())
    	leverage = float(self.leverageTxt.text())
    	price = float(self.priceTxt.text())
    	targetpct = float(self.tpTargetTxt.text())/100
    	bfee= float(self.bfPctTxt.text())
    	sfee= float(self.sfPctTxt.text())
    	tp = findPriceWithFees(price,targetpct,sfee)
    	maxorders = float(self.maxOrdersTxt.text())
    	perorderbgt = balance/maxorders
    	targetProfit = balance*targetpct
    	size = perorderbgt*leverage
    	movetotarget = targetProfit/size
    	movedtotarget = price*movetotarget
    	target = (1+movetotarget)/(1-(bfee+sfee))*size
    	buyfeedollars = size * bfee
    	sellfeedollars = target * sfee

    	self.toBetTxt.setText(str("{:.4f}".format(perorderbgt)))
    	self.sizeTxt.setText(str("{:.4f}".format(size)))
    	self.targetprofitTxt.setText(str("{:.4f}".format(targetProfit)))
    	self.movetotargetTxt.setText(str("{:.4f}".format(movetotarget*100)+'%'))
    	self.movedtotargetTxt.setText(str("{:.4f}".format(movedtotarget)))
    	self.buyfeedTxt.setText(str("{:.4f}".format(buyfeedollars)))
    	self.tpTxt.setText(str("{:.4f}".format(target)))
    	self.sellfeedTxt.setText(str("{:.4f}".format(sellfeedollars)))



# t = returnFee(100)

# print(t)

app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
window = Ui() # Create an instance of our class
app.exec_() # Start the application
