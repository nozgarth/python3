def returnFee(Price= 100,percent=0.0004):
	return {"afterfees" : Price+Price*percent,"fees":Price*percent,"afterFees":Price-Price*percent}

def findPriceWithFees(PriceTarget=100,percent=0.01,fees=0.0004):
	return (1+percent)/(1-fees)*PriceTarget

def findIncreasePercent(start,end):
	return (end-start)/start

def findTargetPrice(price=200,balance = 100, targetPercent=0.02):
	return findPriceWithFees(balance/price,targetPercent)

def balanceIncrease(Balance,profit):
	return{"StartBalance":Balance,"Profit":profit,\
	"Increase":findIncreasePercent(Balance,Balance+profit),\
	"finalBalance":Balance+profit}

def convertToPercent(percent): 
	return( "{:.2%}".format(percent))

print(convertToPercent(findIncreasePercent(56850,55000)))