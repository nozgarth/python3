
from PyQt5 import QtWidgets, uic
import candles
import sys

        #go link--> gobtn
        #list right --> list1
        #output text --> text1
        #selected coin --> cointxt
        #calendar --> cal1
        #date to text --> datetxt
        #timeframe slider --> timeslider
        #timeframe text --> timeframetxt


timeframes= {1:'1m',2:'3m',3:'5m',4:'15m',5:'30m',6:'1h',7:"2h",8:"4h"}
coins=['BTC','ETH','BNB']

class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__() # Call the inherited classes __init__ method
        uic.loadUi('down.ui', self) # Load the .ui file
        self.gobtn.setText("Do")
        self.list1.addItems(coins)
        self.list1.itemClicked.connect(self.returncoin)
        self.btn1.clicked.connect(self.getparams)
        self.cal1.clicked.connect(self.getdate)
        self.timeslider.valueChanged.connect(self.getTimeframe)
        self.show() # Show the GUI

    def getparams(self):
        coinname = self.cointxt.text()
        timeframe = self.timeframetxt.text()
        date=self.datetxt.text()
        #self.text1.append('Downloading Coin:{} Timeframe:{} Date:{}'.format(coinname,timeframe,date))  
        coindata = candles.makenames(coinname,timeframe,date)
        df = candles.downloadCandles(coindata)
        #self.text1.append(str(df))

    def returncoin(self,item):
        self.cointxt.setText(item.text())
        print(item.text())

    def getdate(self,date):
        self.datetxt.setText(str(self.cal1.selectedDate().toPyDate()))

    def getTimeframe(self):
        item = self.timeslider.value() 
        self.timeframetxt.setText(timeframes[item])


app = QtWidgets.QApplication(sys.argv) # Create an instance of QtWidgets.QApplication
window = Ui() # Create an instance of our class
app.exec_() # Start the application