import streamlit as st
import candles
import vars
import plotly.graph_objects as go

fig = go.Figure()


#region
header = st.container()
dataset = st.container()
calc = st.container()
#endregion

with header:
    st.title("Show Charts")

with dataset:
    form = st.sidebar.form("my_form")
    cryptoname=form.selectbox("Crypto",vars.coins)
    selectedDate=form.date_input("Select Date")
    selectedTime=form.selectbox("Fimeframe",vars.timeframes)
    submitted = form.form_submit_button("Get Data")
    if submitted:
        selectedargs = candles.makenames(cryptoname,selectedTime,str(selectedDate))
        with dataset:
            st.write(selectedargs[1])
            df=candles.downloadCandles(selectedargs)
            st.dataframe(df)
            fig.add_trace(go.Candlestick(x=df['DateTime'], open=df['Open'], high=df["High"], low=df['Low'], close=df['Close']) )
            st.plotly_chart(fig)

col1,col2,col3 = st.columns(3)
with col1:
    balance = st.text_input('Balance',160)
    leverage = st.text_input('Leverage',4)
    maxOrders = st.text_input('Max Orders',4)
    neededpct = st.text_input('Target Percent %',3.2)
with col2:    
    tobet = st.text_input('To Spent',str(float(balance)/float(maxOrders)))
    betsize= st.text_input('Size',str((float(balance)/float(maxOrders))*float(leverage))) 


with col3:
    m = st.metric("Take Profit", 4323.34,4)
