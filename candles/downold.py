import pandas as pd
import numpy as np
import io
import requests
import zipfile
import os
import mplfinance as mpf
print ("OK")

def dlHistoricData(coin='BTC',timeframe="5m",month="01",year="2021"):
    url = 'https://data.binance.vision/data/futures/um/monthly/klines/'
    coinpref= coin+'USDT'
    filename = coinpref+'-'+timeframe+'-'+year+'-'+month+'.zip'
    try:
        df = pd.read_csv(url+coinpref+'/'+timeframe+'/'+filename)
        cols = [5,6,8,9,10,11]
        df.drop(df.columns[cols],axis=1,inplace=True)
        df.to_pickle('data\\'+filename.rsplit( ".", 1 )[ 0 ])
        return df
    except:
        print("There is no Data!")

def getAllData(coinName):
    coinsToDl = [coinName]#,'ADA','XMR','DASH','ZEC','XTZ','ATOM','ONT','IOTA','BAT','VET','NEO','QTUM','IOST']
    timeframesToDl=['5m']#,'15m','30m','1h','2h','4h','1d']
    monthsToDl = ['01']#,'02','03','04','05','06','07','08','09','10']

    for coin in coinsToDl:
        for time in timeframesToDl:
            for mo in monthsToDl:
                dlHistoricData(coin,time,mo)
                print('Downloading...',coin, mo, time)
    print("Done!!")

def loadData(coin='BTC',timeframe="5m",month="01",year="2021"):
    coinpref= coin+'USDT'
    filename = coinpref+'-'+timeframe+'-'+year+'-'+month
    print(filename)
    if(os.path.isfile('data//'+filename)):
        print("There is a file!")
        df = pd.read_pickle('data//'+filename)
        return(df)
    else :
        print("NO File Found...trying to Download...")
        getAllData(coin)

df= loadData('BTC','4h','10')
df.columns=['Time','Open','High','Low','Close','Volume']
df['Time'] = pd.to_datetime(df['Time'], unit="ms")

df = df.set_index('Time')
df.info()

mpf.plot(df,type='candle',volume=True,mav=21,tight_layout=True,style="yahoo")


