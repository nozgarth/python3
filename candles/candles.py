import pandas as pd
import os.path
from datetime import date
from datetime import timedelta

def downloadCandles(coindata):
	try:	
		filepath = "./data/"+coindata[1]
		if loadIfExists(filepath):
			df= pd.read_pickle(filepath)
			#df = df.drop(df.columns[[6,7,8,9,10,11]],axis=1)
			#df.columns= ['DateTime','Open','High','Low','Close','Volume']
			return df
		else:

			df= pd.read_csv(coindata[0])
			df = df.drop(df.columns[[6,7,8,9,10,11]],axis=1)
			df.columns= ['DateTime','Open','High','Low','Close','Volume']
			df.to_pickle(filepath)
			return df
	except:
		print(coindata[0])
		print(coindata[1])
		return("Cant find file!")	
		
def loadIfExists(filename):
	if os.path.isfile(filename):
		print ("File Found")
		return True
	else:
		print("Not Found!")
		return False

def makenames(coin="BTC",timeFrame = '15',date='1/1/2020'):
	coinstring= coin+'USD_PERP'
	filename = coinstring+'-'+timeFrame+'-'+date
	urlstring = 'https://data.binance.vision/data/futures/cm/daily/klines/'
	urlstring= urlstring+ coinstring+'/'
	urlstring = urlstring + timeFrame+'/'
	urlstring = urlstring+ filename+'.zip'
	return ([urlstring,filename])

