BALANCE = 100
BUYPRICE = 5.774
SELLPRICE = 5.795
LEVERAGE = 15
MSIZE = 100
BFEE = 0.0004
SFEE = 0.0004
MAXORDERS = 2
POSITION = "LONG"


class longorder():
    def __init__(self,balancep = BALANCE,leveragep=LEVERAGE,maxordersp=MAXORDERS,bpricep=BUYPRICE,spricep=SELLPRICE,bfeep=BFEE,sfeep=SFEE):
        self.balance = balancep
        self.leverage = leveragep
        self.maxorders = maxordersp
        self.bprice = bpricep
        self.sprice = spricep
        self.bfee= bfeep
        self.sfee = sfeep
        self.amount = self.balance / self.maxorders
        self.size = self.amount * self.leverage
       
        returnquote = self.size/ self.bprice
        bfee = returnquote * self.bfee
        freturn= returnquote - bfee
        sreturn = freturn * self.sprice
        sfee = sreturn * self.sfee
        sreturn = sreturn - sfee
        
        self.profit = sreturn - self.size
        self.moved = self.profit / self.size
        self.move = '{0:.3g}'.format(self.moved)
        self.profitpct = self.moved * self.leverage

        
        print(f"Balance:{self.balance} Amount:{self.amount}")
        print(f"Buy Price:{self.bprice} Sell Price:{self.sprice}")
        print(f"Size:{self.size}")
        print(f"Leverage:{self.leverage} Returned:{sreturn}")
        print(f"Profit: {self.profit} Move:{self.move*100} Pct%:{self.profitpct*100}"  )


bt= longorder(19.329)
        
